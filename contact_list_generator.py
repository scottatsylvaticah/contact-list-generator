# contact_list_generator.py
# Copyright 2021-2024 Scott Houston. All rights reserved.

# Process a list of phone numbers so that they can be imported as
# Google contacts.

# Process phone numbers so that:
#   anything that's not a phone number gets chucked,
#   there are no duplicates within the provided list,
#   there are no duplicates when compared against the master number list, and
#   numbers are properly formatted for use by Google.

# Write numbers to a new/replacement contacts list file.
# Append numbers to the master list in order to avoid future duplicates.

# This program requires the phonenumbers library. To install, run:
# pip3 install phonenumbers

from pathlib import Path
import phonenumbers

number_list = []
input_file = "numberlist.csv"
master_file = "master_number_list.csv"
output_file = "contact_list.csv"

if not Path(input_file).is_file():
	print("Unable to find numberlist.csv")
	print("It should be in the same directory as this program.")
	exit(1)

# Read in all phone numbers to be processed.
# If what's read is considered to be a phone number, format the number
# and add it to the number list.
with open(input_file, "r") as number_file:
	for line in number_file:
		try:
			possible_number = phonenumbers.parse(line, region="US")
			if phonenumbers.is_valid_number(possible_number):
				formatted_number = phonenumbers.format_number(possible_number, phonenumbers.PhoneNumberFormat.NATIONAL)
				number_list.append(formatted_number)
		except phonenumbers.phonenumberutil.NumberParseException:
			# Ignore this exception because it means that we simply don't
			# have a valid phone number.
			pass

# Eliminate duplicate numbers within the provided list.
clean_list = []
for number in number_list:
	if number not in clean_list:
		clean_list.append(number)

# Eliminate duplicates by comparing the clean list against the master phone
# number list.
# This gets skipped if is an initial run and a master phone number list
# doesn't exist yet.
if Path(master_file).is_file():
	with open(master_file, "r") as master_list:
		for master_number in master_list:
			stripped_master_number = master_number.strip()
			if stripped_master_number in clean_list:
				clean_list.remove(stripped_master_number)

# Write numbers to the contacts list file
with open(output_file, "w+") as contacts_file:
	contacts_file.write("Phone\n")
	for number in clean_list:
		contacts_file.write("%s\n" % number)

# Add the numbers to the master phone number list file.
with open(master_file, "a+") as output_file:
	for number in clean_list:
		output_file.write("%s\n" % number)